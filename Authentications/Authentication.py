from fastapi import Depends, FastAPI, Header, HTTPException
from Bridges.AuthenticationService import is_auth
from functools import wraps


async def is_login(Id: str = Header(...), Token: str = Header(...)):
    result = is_auth(Id, Token)
    if result:
        return result
    else:
        raise HTTPException(status_code=403, detail="Login Required")


async def get_token_header(x_token: str = Header(...), x_id: str = Header(...)):
    if x_token != "fake-super-secret-token":
        raise HTTPException(status_code=400, detail="X-Token header invalid")


def permission(permission_name: str):
    def real_decorator(func):
        @wraps(func)
        def wrapper(*args, **kwargs):
            # todo : permission check impl
            # permission check
            return func(*args, **kwargs)

        return wrapper

    return real_decorator
