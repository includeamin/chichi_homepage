from fastapi import FastAPI, HTTPException
from starlette.responses import HTMLResponse, JSONResponse
from starlette.middleware.cors import CORSMiddleware

from API.HeaderSliderApi import header_slider_route
from API.QueryTypesApi import query_type_route
from API.HomePageApi import homepage_route
from API.BannerApi import banner_route
from API.PackageApi import package_route
from API.CategoryApi import category_route
from API.SliderApi import slider_route
from API.ItemListApi import item_list_route
from API.HomePageApi import homepage_route
from API.HomePageApi import add_home_route
from uvicorn import run
from Test.HomePageHardCode import home_page
from DataBase.DB import homepage_collection

app = FastAPI()

origins = [
    "http://localhost.tiangolo.com",
    "https://localhost.tiangolo.com",
    "http://localhost",
    "http://localhost:8080",
    "http://localhost:3000"
]
app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)


@app.exception_handler(HTTPException)
async def http_exception(request, exc):
    return JSONResponse({"detail": exc.detail}, status_code=exc.status_code)


# assign routes
app.include_router(header_slider_route)
app.include_router(query_type_route)
app.include_router(homepage_route)
app.include_router(banner_route)
app.include_router(package_route)
app.include_router(category_route)
app.include_router(slider_route)
app.include_router(item_list_route)
app.include_router(homepage_route)
app.include_router(add_home_route)


# test
# add middleware
# app.add_middleware(
#     add_process_time_header
# )
# @app.middleware("http")
# async def add_process_time_header(request: Request, call_next):
#     print(request.headers["Id"], request.headers["Token"])
#     response = await call_next(request)
#     response.headers["X-Process-Time"] = 335
#     return response


@app.get("/", tags=["Main"])
def index():
    # homepage_collection.insert_one(home_page)

    return {"Email": "aminjamaml10@gmail.com", "Name": "amin jamal", "Nickname": "includeamin"}


@app.get('/test', tags=["Main"])
def test():
    aggr_result = homepage_collection.aggregate([
        {"$project": {f'Body.Position': 1}},
        {"$unwind": "$Body"},
        {"$sort": {"Body.Position": -1}},
        {"$limit": 1}

    ])
    max_index = [item for item in aggr_result]["Body"]
    print(max_index)

    return "ok"


@app.get('/healthz')
def check_liveness():
    inv = homepage_collection.find_one({}, {})
    return 'd'


if __name__ == '__main__':
    run(app, host='0.0.0.0', port=3006, debug=True, loop='asyncio')
