from fastapi import APIRouter, Depends, Form
from Authentications.Authentication import is_login
from Authentications.Authentication import permission
from Classes.Categories import Category
from Models.CategoryModel import ImageList, AddCategoryResponseModel, GetAllCategoryResponseModel, AddCategoryModel, \
    DeleteCategoryResponse, CategoryModel

category_route = APIRouter()


@category_route.post("/admin/category/add", dependencies=[Depends(is_login)], response_model=AddCategoryResponseModel,
                     tags=["Category"], description="Add category basic with name")
@permission("HomePageEdit")
def add(Name: str = Form(...)):
    return Category.add(Name)


@category_route.put("/admin/category/{category_id}/items/update", dependencies=[Depends(is_login)],
                    description="add item of category", tags=["Category"],
                    response_model=AddCategoryResponseModel)
@permission("HomePageEdit")
def update_item(category_id,
                Position: int = Form(...),
                Image: str = Form(...),
                DestinationId: str = Form(...)
                ):
    return Category.add_item(category_id,
                             ImageList(**{"Image": Image, "Position": Position, "DestinationId": DestinationId}))


@category_route.get("/admin/categories", dependencies=[Depends(is_login)], description="Get all of categories",
                    tags=["Category"],
                    response_model=GetAllCategoryResponseModel)
@permission("HomePageEdit")
def get_all():
    return Category.get_all()


@category_route.get("/admin/category/{category_name}", dependencies=[Depends(is_login)],
                    description="Get category by name", tags=["Category"],
                    response_model=CategoryModel)
@permission("HomePageEdit")
def get(category_name: str):
    return Category.get(category_name)


@category_route.delete("/admin/category/{category_name}/delete", dependencies=[Depends(is_login)],
                       description="Delete category by id", tags=["Category"],
                       response_model=DeleteCategoryResponse)
@permission("HomePageEdit")
def delete(category_name: str):
    return Category.delete(category_name)

