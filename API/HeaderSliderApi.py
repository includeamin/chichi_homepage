from Classes.HeaderSlider import HeaderSlider
from fastapi import APIRouter, Depends, Form
from Authentications.Authentication import is_login, permission
from Models.HeaderSliderModel import HeaderSliderModel, AddHeaderSliderModel, ImageList, GetAllHeaderSliderModel, \
    DeleteHeaderSliderModel, GetValidBannerDestination
from Models.ResponseModel.HeaderSliderResponseModel import Model, AdminGetAllMode, AdminDeleteModel, AdminGetModel

header_slider_route = APIRouter()


@header_slider_route.get('/admin/header/banners/destinations', description="Get valid destinations",
                         tags=["Header Slider"],
                         response_model=GetValidBannerDestination)
@permission("HomePageEdit")
def get_valid_destination():
    return HeaderSlider.get_valid_banners_destination()


@header_slider_route.post("/admin/header/slider/add", dependencies=[Depends(is_login)],
                          description="Add slider with name",
                          tags=["Header Slider"],
                          response_model=AddHeaderSliderModel)
@permission("HomePageEdit")
def add(Name: str = Form(...), Number: int = Form(...)):
    return HeaderSlider.add(header_slider_name=Name, number=Number)


# dependencies=[Depends(is_login)]
@header_slider_route.put("/admin/header/slider/{slider_name}/items/update",
                         dependencies=[Depends(is_login)],
                         description="add items to header slider with name",
                         tags=["Header Slider"],
                         response_model=AddHeaderSliderModel)
@permission("HomePageEdit")
def update_item(slider_name,
                Position: int = Form(...),
                Image: str = Form(...),
                DestinationId: str = Form(...),
                Destination: str = Form(...)
                ):
    return HeaderSlider.add_item(header_slider_name=slider_name,
                                 item_list=ImageList(**{"Image": Image, "Position": Position,
                                                        "DestinationId": DestinationId,
                                                        "Destination": Destination}))


@header_slider_route.get("/admin/header/sliders", description="Get all of sliders",
                         dependencies=[Depends(is_login)],
                         tags=["Header Slider"],
                         response_model=GetAllHeaderSliderModel)
@permission("HomePageEdit")
def get_all():
    return HeaderSlider.get_all()


@header_slider_route.get("/admin/header/slider/{slider_name}",
                         dependencies=[Depends(is_login)],
                         description="Get slider by name", tags=["Header Slider"],
                         response_model=HeaderSliderModel)
@permission("HomePageEdit")
def get(slider_name):
    return HeaderSlider.get(slider_name)


@header_slider_route.delete("/admin/header/slider/{slider_name}/delete",
                            dependencies=[Depends(is_login)],
                            description="Delete slider by name", tags=["Header Slider"],
                            response_model=DeleteHeaderSliderModel)
@permission("HomePageEdit")
def delete(slider_name: str):
    return HeaderSlider.delete(slider_name)
