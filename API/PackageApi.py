from fastapi import APIRouter, Depends, Form
from Authentications.Authentication import is_login
from Authentications.Authentication import permission
from Classes.Packages import Package
from Models.PackageModel import AddPackageRequest, PackageModel, ImageList, GetAllPackageResponse, DeletePackageRequest
from typing import List

package_route = APIRouter()


@package_route.post("/admin/packages/add", dependencies=[Depends(is_login)], response_model=AddPackageRequest,
                    tags=["Package"],
                    description="Add package")
@permission("HomePageEdit")
def add(Name: str = Form(...)):
    return Package.add(Name)


@package_route.put("/admin/packages/{package_id}/items/update", dependencies=[Depends(is_login)], tags=["Package"],
                   description="Update items of Package")
@permission("HomePageEdit")
def update(package_id,
           Position: int = Form(...),
           Image: str = Form(...),
           DestinationId: str = Form(...)):
    return Package.add_item(package_id,
                            ImageList(**{"Image": Image, "Position": Position, "DestinationId": DestinationId}))


@package_route.get("/admin/package/{package_name}", dependencies=[Depends(is_login)],
                   description="Get package with package name", tags=["Package"],
                   response_model=PackageModel)
@permission("HomePageEdit")
def get(package_name):
    return Package.get(package_name)


@package_route.get("/admin/packages", dependencies=[Depends(is_login)], description="Get all packages",
                   tags=["Package"],
                   response_model=GetAllPackageResponse)
@permission("HomePageEdit")
def get_all():
    return Package.get_all()


@package_route.delete("/admin/package/{package_id}", dependencies=[Depends(is_login)],
                      response_model=DeletePackageRequest,
                      description="Delete package with package id", tags=['Package'])
@permission("HomePageEdit")
def delete(package_id):
    return Package.delete(package_id)
