from fastapi import APIRouter, Depends, Form
from Authentications.Authentication import is_login
from Authentications.Authentication import permission
from Models.RequestModel.BannerRequest import Add
from Models.ResponseModel.Banner import Add as AddResponse, GetAll, Delete
from Models.BannerModel import BannerModel, GetValidBannerDestination
from Classes.Banners import Banners

banner_route = APIRouter()


@banner_route.get('/banners/destinations', description="Get valid destinations", tags=["Banner"],
                  response_model=GetValidBannerDestination)
@permission("HomePageEdit")
def get_valid_destination():
    return Banners.get_valid_banners_destination()


# , dependencies=[Depends(is_login)]
@banner_route.post("/banners/add", dependencies=[Depends(is_login)], response_model=AddResponse,
                   description="Add New Banner",
                   tags=["Banner"])
@permission("HomePageEdit")
def add(Name: str = Form(...),
        Image: str = Form(...),
        Destination: str = Form(...),
        DestinationId: str = Form(...)):
    return Banners.add(
        Add(**{"Name": Name, "Image": Image, "Destination": Destination, "DestinationId": DestinationId}))


@banner_route.get("/banners/{banner_name}", dependencies=[Depends(is_login)], response_model=BannerModel,
                  description="Get banner by Id", tags=["Banner"])
@permission("HomePageEdit")
def get(banner_name):
    return Banners.get(banner_name)


@banner_route.delete("/banners/{banner_id}", dependencies=[Depends(is_login)],
                     description="Delete banner with banner id", tags=["Banner"],
                     response_model=Delete)
@permission("HomePageEdit")
def delete(banner_id):
    return Banners.delete(banner_id)


@banner_route.get("/banners", dependencies=[Depends(is_login)], description="Get all banners", tags=["Banner"],
                  response_model=GetAll)
@permission("HomePageEdit")
def get_all():
    print("aminjamal")
    return Banners.get_all()
