from fastapi import APIRouter, Form
from Test.HomePageHardCode import home_page
from Classes.QueryTypes import Query
from Models.HomePageModel import FinalHomePageModel, HomePageModel, ComponentModel, AddHomePageResponseModel, \
    AddComponentModel, UpdateHomePageWithJsonModel, GetAbvailableObjectType, GetAbvailableScreenPosition, \
    DeleteHomePageResponseModel, HomePageActiveModel, GetAllHomePageModel, List
from Classes.HomePage import HomePage
import json

homepage_route = APIRouter()
add_home_route = APIRouter()


@homepage_route.get('/homepage', tags=["FinalHomePageModel"])
async def get_home_page_temp():
    return HomePage.mobile_load_homepage()
    # for item in home_page["Body"]:
    #     if item["ObjectType"] == "ItemList":
    #         item["Data"]["Data"] = Query.query_execution_by_key("Best", 'ItemList', "", "id")
    # return home_page


@homepage_route.post("/admin/homepage/init/{homepage_name}", description="Init new Homepage with Name",
                     tags=["HomePage"],
                     response_model=AddHomePageResponseModel)
def init_new_homepage(homepage_name: str):
    return HomePage.init_new_homepage(home_page_name=homepage_name)


# todo : change input scheme

# @add_home_route.put("/admin/homepage/update", description="Update homepage with homepage json", tags=["HomePage"],
#                     response_model=AddHomePageResponseModel)
# def update_home_page_json(Json: UpdateHomePageWithJsonModel = Form(...)):
#     return HomePage.update_homepage_with_json(Json)
#
# @add_home_route.put("/admin/homepage/{homepage_name}/update", description="Update homepage with homepage json", tags=["HomePage"],
#                     response_model=AddHomePageResponseModel)
# def update_home_page_json(homepage_name:str ,Data:str =Form(...)):
#     Data = json.loads(Data)
#     return HomePage.update_homepage_with_json(UpdateHomePageWithJsonModel(**{"Name":homepage_name,"Header":Data["Header"],
#                                                                              "Body":Data["Body"],"Footer":Data["Footer"]}))


@add_home_route.put("/admin/homepage/update", description="Update homepage with homepage json", tags=["HomePage"],
                    response_model=AddHomePageResponseModel)
def update_home_page_json(Data: UpdateHomePageWithJsonModel):
    # Data = json.loads(Data)
    return HomePage.update_homepage_with_json(UpdateHomePageWithJsonModel(**{"Name": Data.Name, "Header": Data.Header,
                                                                             "Body": Data.Body, "Footer": Data.Footer}))


@homepage_route.get("/admin/homepage/object-types", description="Get all ObjectTypes", tags=["HomePage"],
                    response_model=GetAbvailableObjectType)
def get_available_object_types():
    return HomePage.get_available_object_type()


@homepage_route.get("/admin/homepage/screen-position", description="Get all available posiition screen",
                    response_model=GetAbvailableScreenPosition, tags=["HomePage"])
def get_available_position():
    return HomePage.get_available_screen_position()


@homepage_route.get("/admin/homepage/{homepage_name}", description="Get Homepage by name", tags=["HomePage"],
                    response_model=HomePageModel)
def get_home_page_by_name(homepage_name: str):
    return HomePage.get_home_page_by_name(homepage_name)


@homepage_route.delete("/admin/homepage/{homepage_name}", description="Delete Homepage by name", tags=["HomePage"],
                       response_model=DeleteHomePageResponseModel)
def delete_homepage_by_name(homepage_name: str):
    return HomePage.delete(homepage_name)


@homepage_route.put("/admin/homepage/{homepage_name}/active", description="Active HomePage by name", tags=["HomePage"],
                    response_model=HomePageActiveModel)
def active(homepage_name: str):
    return HomePage.active(homepage_name)


@homepage_route.get("/admin/homepage/{homepage_name}/load", description="Load Homepage by homepage name",
                    tags=["HomePage"])
def load(homepage_name: str):
    return HomePage.load_home_page(homepage_name)


@homepage_route.get("/admin/homepages", description="Get All Homepages", tags=["HomePage"],
                    response_model=GetAllHomePageModel)
def get_all():
    return HomePage.get_all_homepage()

# @homepage_route.post("/admin/homepage/content/update", description="Update homepage with homepage json",
#                      tags=["HomePage"],
#                 )
# def add_json(Title: str = Form(...)):
#     print(Title)
#     # return HomePage.update_homepage_with_json(Json)
#

# @add_home_route.post("/homepage/content/add-content")
# def update(amin: str = Form(...)):
#     return "amin"
