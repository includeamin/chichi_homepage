from fastapi import APIRouter, Depends, Form
from Authentications.Authentication import is_login
from Authentications.Authentication import permission
from Models.ResponseModel.QueryTypes import AdminGetAllQueryType, AdminAddQueryType
from Models.QueryTypeModel import GetKeysModel
from Classes.QueryTypes import Query

query_type_route = APIRouter()


@query_type_route.post('/admin/query/types/add', dependencies=[Depends(is_login)], response_model=AdminAddQueryType,
                       tags=["QueryType"], description="add query type")
@permission('HomePageEdit')
def admin_add(Key: str = Form(...), Service: str = Form(...), Url: str = Form(...), ObjectType: str = Form(...)):
    return Query.admin_add(key=Key, service=Service, url=Url, object_type=ObjectType)




@query_type_route.get('/admin/query/types', dependencies=[Depends(is_login)], response_model=AdminGetAllQueryType,
                      tags=["QueryType"], description='admin get all queries')
@permission('HomePageEdit')
def admin_get_all():
    return Query.admin_get_all()


@query_type_route.get('/admin/test/query', tags=["QueryType"])
def test():
    return Query.available_keys()
    #return Query.query_execution_by_key("Best", 'ItemList', "", "id")


@query_type_route.get("/admin/query/keys",description="get available query keys ",

                      tags=["HomePage","QueryType"],response_model=GetKeysModel, dependencies=[Depends(is_login)])
def get_available_keys():
    return Query.available_keys()

