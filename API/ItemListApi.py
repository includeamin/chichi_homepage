from fastapi import APIRouter, Depends, Form
from Authentications.Authentication import is_login
from Authentications.Authentication import permission
from Models.ItemListModel import ItemListModel, LoadItemListModel, AddItemListResponseModel, AddItemListModel, \
    DeleteItemListResponseModel
from Classes.ItemLists import ItemList

item_list_route = APIRouter()


@item_list_route.post("/admin/item-list/add", dependencies=[Depends(is_login)], description="Add item list by title",
                      tags=["ItemList"],
                      response_model=AddItemListResponseModel)
@permission("HomePageEdit")
def add(Title: str = Form(...),
        QueryKey: str = Form(...)):
    print(Title)
    return ItemList.add(AddItemListModel(**{"Title": Title, "QueryKey": QueryKey}))


@item_list_route.get("/admin/item-list/{title}/get", dependencies=[Depends(is_login)], description="Get item list",
                     tags=["ItemList"], response_model=ItemListModel)
@permission("HomePageEdit")

def get(title: str):
    return ItemList.get_by_title(title)


@item_list_route.get("/admin/item-list/{title}/get/loaded", dependencies=[Depends(is_login)],
                     description="Get loaded of item-list by title"
    , tags=["ItemList"], response_model=LoadItemListModel)
@permission("HomePageEdit")

def get_loaded(title: str):
    return ItemList.get_by_title_load(title)


@item_list_route.delete("/admin/item-list/{title}/delete", dependencies=[Depends(is_login)],
                        description="Delete item list by id ",
                        tags=["ItemList"], response_model=DeleteItemListResponseModel)
@permission("HomePageEdit")

def delete(title: str):
    return ItemList.delete(title)

@item_list_route.get("/admin/item-lists",dependencies=[Depends(is_login)],description="get all item lists",tags=["ItemList"])
@permission("HomePageEdit")

def get_all():
    return ItemList.get_all()
