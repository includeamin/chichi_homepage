from fastapi import APIRouter, Depends, Form
from Authentications.Authentication import is_login
from Authentications.Authentication import permission
from Classes.Slider import Slider
from Models.SliderModel import ImageList, AddSliderResponse, DeleteSliderResponse, SliderModel, GetAllSliderResponse, \
    GetValidBannerDestination

slider_route = APIRouter()


@slider_route.get('/admin/slider/destinations', description="Get valid destinations", tags=["Slider"],
                  response_model=GetValidBannerDestination)
@permission("HomePageEdit")
def get_valid_destination():
    return Slider.get_valid_banners_destination()


@slider_route.post("/admin/slider/add", dependencies=[Depends(is_login)], description="Add slider with name",
                   tags=["Slider"],
                   response_model=AddSliderResponse)
@permission("HomePageEdit")
def add(Name: str = Form(...), Number:int=Form(...)):
    return Slider.add(slider_name=Name,number=Number)


@slider_route.put("/admin/slider/{slider_name}/items/update", dependencies=[Depends(is_login)],
                  description="add items to slider with name",
                  tags=["Slider"],
                  response_model=AddSliderResponse)
@permission("HomePageEdit")
def update_item(slider_name,
                Position: int = Form(...),
                Image: str = Form(...),
                DestinationId: str = Form(...),
                Destination: str = Form(...)
                ):
    return Slider.add_item(slider_name=slider_name, item_list=ImageList(**{"Image": Image, "Position": Position,
                                                                           "DestinationId": DestinationId,
                                                                           "Destination": Destination}))


@slider_route.get("/admin/sliders", dependencies=[Depends(is_login)], description="Get all of sliders",
                  tags=["Slider"],
                  response_model=GetAllSliderResponse)
@permission("HomePageEdit")
def get_all():
    return Slider.get_all()


@slider_route.get("/admin/slider/{slider_name}", dependencies=[Depends(is_login)],
                  description="Get slider by name", tags=["Slider"],
                  response_model=SliderModel)
@permission("HomePageEdit")
def get(slider_name: str):
    return Slider.get(slider_name)


@slider_route.delete("/admin/slider/{slider_name}/delete", dependencies=[Depends(is_login)],
                     description="Delete slider by name", tags=["Slider"],
                     response_model=DeleteSliderResponse)
@permission("HomePageEdit")
def delete(slider_name: str):
    return Slider.delete(slider_name)
