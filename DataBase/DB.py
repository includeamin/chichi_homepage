import pymongo
import json
import os
import requests
from Classes.Tools import Tools
import logging

print("here")
Decode = Tools.decode
Initer = Tools.initer

dirpath = os.getcwd()

DEBUG = False
with open(os.path.join(dirpath, "./Configs/Configs.json")) as f:
    configs = json.load(f)

if not DEBUG:

    get_database_url = json.loads(
        requests.get("{0}/Services/config/get/HomePage".format(configs['MicroServiceManagementURl']),
                     verify=False).content)
    if not get_database_url["State"]:
        exit(1)

    get_database_url = json.loads(get_database_url["Description"])

    url = Decode(get_database_url["DatabaseString"], get_database_url["Key"][2:-1])

else:
    url = 'mongodb://localhost:27017'


#url  = "mongodb://includeamin:aminjamal@chichiapp.ir:32032,chichiapp.ir:32033,chichiapp.ir:32034/?replicaSet=rs0"
try:
    mongodb = pymongo.MongoClient(url)
    print(mongodb)
except:
    mongodb = pymongo.MongoClient(url)

database = mongodb[configs["Database"]["Name"]]
homepage_collection = database[configs["Database"]["Collections"]["HomePage"]]
final_homepage_collection = database[configs["Database"]["Collections"]["FinalHomePage"]]
header_slider_collection = database[configs["Database"]["Collections"]["HeaderSlider"]]
slider_collection = database[configs["Database"]["Collections"]["Slider"]]
category_collection = database[configs["Database"]["Collections"]["Category"]]
item_list_collection = database[configs["Database"]["Collections"]["ItemList"]]
package_collection = database[configs["Database"]["Collections"]["Package"]]
inprogress_order_collection = database[configs["Database"]["Collections"]["InProgressOrder"]]
banner_collection = database[configs["Database"]["Collections"]["Banner"]]
query_types_collection = database[configs["Database"]["Collections"]["QueryTypes"]]


