from pydantic import BaseModel
from typing import List
from datetime import datetime


class ImageList(BaseModel):
    Image: str = None
    Position: int
    DestinationId: str = None
    Create_at: datetime = datetime.now()
    Update_at: datetime = datetime.now()


class PackageModel(BaseModel):
    Name: str
    Items: List[ImageList] = [
        ImageList(Position=0),
        ImageList(Position=1),
        ImageList(Position=2),
        ImageList(Position=3),
        ImageList(Position=4)
    ]
    IsUsed: bool = False
    Create_at: datetime = datetime.now()
    Update_at: datetime = datetime.now()




class AddPackageRequest(BaseModel):
    ItemId: str

class DeletePackageRequest(BaseModel):
    ItemId: str


class GetAllPackageResponse(BaseModel):
    Items: list


