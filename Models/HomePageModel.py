from pydantic import BaseModel
from typing import List
from datetime import datetime


class DataModel(BaseModel):
    Title: str
    Data: list


class AddComponentModel(BaseModel):
    Id: str
    ObjectType: str


class ComponentModel(BaseModel):
    ObjectType: str = "ItemList"
    Position: int = 0
    Data: DataModel = {"Title": "Title", "Data": []}


class FinalHomePageModel(BaseModel):
    Name: str
    Header: List[ComponentModel]
    Body: List[ComponentModel]
    Footer: List[ComponentModel]
    Create_at: datetime = datetime.now()


class HomePageModel(BaseModel):
    Name: str
    Header: List[ComponentModel] = []
    Body: List[ComponentModel] = []
    Footer: List[ComponentModel] = []
    Create_at: datetime = datetime.now()
    Update_at: datetime = datetime.now()
    IsUsed: bool = False


class AddHomePageResponseModel(BaseModel):
    ItemId: str


class DeleteHomePageResponseModel(BaseModel):
    ItemId: str


class UpdateHomePageWithJsonModel(BaseModel):
    Name: str
    Header: list = []  # List[ComponentModel] = []
    Body: list = []  # List[ComponentModel] = []
    Footer: list = []  # List[ComponentModel] = []


class GetAbvailableObjectType(BaseModel):
    Items: list


class GetAbvailableScreenPosition(BaseModel):
    Items: list


class HomePageActiveModel(BaseModel):
    Action: str = "Active or deactive"


class GetAllHomePageModel(BaseModel):
    Items: list
