from pydantic import BaseModel
from datetime import datetime


class ItemListModel(BaseModel):
    Title: str
    QueryKey: str
    Create_at: datetime = datetime.now()
    Update_at: datetime = datetime.now()


class LoadItemListModel(BaseModel):
    Title: str
    Data: list


class AddItemListModel(BaseModel):
    Title: str
    QueryKey: str


class AddItemListResponseModel(BaseModel):
    ItemId: str


class DeleteItemListResponseModel(BaseModel):
    ItemId: str

class ItemListComponentDataModel(BaseModel):
    Title: str
    Data: list
