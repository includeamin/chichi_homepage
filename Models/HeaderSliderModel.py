from pydantic import BaseModel
from datetime import datetime
from typing import List


class ImageList(BaseModel):
    Image: str = None
    Position: int
    DestinationId: str = None
    Destination: str = None


# class HeaderSliderModel(BaseModel):
#     Image: str
#     Destination: str
#     DestinationId: str
#     Create_at: datetime = datetime.now()
#     IsUsed: bool = False


class HeaderSliderModel(BaseModel):
    Name: str
    Items: List[ImageList] = []
    Create_at: datetime = datetime.now()
    IsUsed: bool = False

class AddHeaderSliderModel(BaseModel):
    ItemId: str

class DeleteHeaderSliderModel(BaseModel):
    ItemId: str

class GetAllHeaderSliderModel(BaseModel):
    Items: list

class GetValidBannerDestination(BaseModel):
    ValidDestination: list
