from pydantic import BaseModel
from datetime import datetime
from typing import List


class ImageList(BaseModel):
    Image: str = None
    Position: int
    DestinationId: str = None
    Destination: str = None


class SliderModel(BaseModel):
    Name: str
    Items: List[ImageList] = []
    Create_at: datetime = datetime.now()
    Update_at: datetime = datetime.now()
    IsUsed: bool = False


class AddSliderResponse(BaseModel):
    ItemId: str


class GetAllSliderResponse(BaseModel):
    Items: list

class DeleteSliderResponse(BaseModel):
    ItemId: str

class GetValidBannerDestination(BaseModel):
    ValidDestination: list



