from pydantic import BaseModel
from datetime import datetime


class BannerModel(BaseModel):
    Name: str
    Image: str
    Destination: str
    DestinationId: str
    Create_at: datetime = datetime.now()
    Update_at: datetime = datetime.now()
    IsUsed: bool = False

class GetValidBannerDestination(BaseModel):
    ValidDestination: list
