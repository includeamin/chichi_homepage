from pydantic import BaseModel
from datetime import datetime


class QueryTypeModel(BaseModel):
    ObjectType: str
    Key: str
    Service: str
    Url: str
    Create_at : datetime = datetime.now()
    Update_at : datetime = datetime.now()


class GetKeysModel(BaseModel):
    Keys: list
