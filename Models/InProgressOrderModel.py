from pydantic import BaseModel
from datetime import datetime


class InProgressModel(BaseModel):
    StateLogo: str
    EstimatedTime: datetime
    OrderNumber: str
    OrderStatus: str
