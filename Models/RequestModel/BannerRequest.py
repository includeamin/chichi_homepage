from pydantic import BaseModel


class Add(BaseModel):
    Name: str
    Image: str
    Destination: str
    DestinationId: str
