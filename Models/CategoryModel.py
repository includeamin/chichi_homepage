from pydantic import BaseModel
from datetime import datetime
from typing import List


class ImageList(BaseModel):
    Image: str = None
    Position: int
    DestinationId: str = None
    Create_at: datetime = datetime.now()
    Update_at: datetime = datetime.now()


class CategoryModel(BaseModel):
    Name: str
    Items: List[ImageList] = [
        ImageList(Position=0),
        ImageList(Position=1),
        ImageList(Position=2),
        ImageList(Position=3),
    ]
    Create_at: datetime = datetime.now()
    Update_at: datetime = datetime.now()
    IsUsed: bool = False


class AddCategoryModel(BaseModel):
    Name: str


class AddCategoryResponseModel(BaseModel):
    ItemId: str


class GetAllCategoryResponseModel(BaseModel):
    Items: list


class DeleteCategoryResponse(BaseModel):
    ItemId: str
