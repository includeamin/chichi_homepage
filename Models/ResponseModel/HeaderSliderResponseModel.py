from pydantic import BaseModel
from Models.HeaderSliderModel import HeaderSliderModel
from datetime import datetime


class Model(BaseModel):
    ItemId: str


class AdminGetAllMode(BaseModel):
    Headers: list

class AdminGetByIdModel(BaseModel):
    Header: HeaderSliderModel


class AdminDeleteModel(BaseModel):
    Message: str = "Done"

class AdminGetModel(BaseModel):
    Header: HeaderSliderModel

