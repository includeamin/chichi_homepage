from pydantic import BaseModel


class Add(BaseModel):
    ItemId: str


class Delete(BaseModel):
    ItemId: str

class GetAll(BaseModel):
    Items: list
