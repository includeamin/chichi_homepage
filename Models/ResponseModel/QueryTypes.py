from pydantic import BaseModel

class AdminAddQueryType(BaseModel):
    QueryId: str
class AdminGetAllQueryType(BaseModel):
    Queries: list