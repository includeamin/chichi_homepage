from Classes.Tools import Tools
from Models.ItemListModel import ItemListModel, AddItemListResponseModel, DeleteItemListResponseModel, \
    AddItemListModel, LoadItemListModel
from Classes.QueryTypes import Query
from DataBase.DB import item_list_collection
from fastapi import HTTPException
from Classes.QueryTypes import Query
from bson.objectid import ObjectId


class ItemList:
    @staticmethod
    def add(item: AddItemListModel):
        if not Query.available_keys().Keys.__contains__(item.QueryKey):
            raise HTTPException(detail="Invalid query key", status_code=400)

        same = item_list_collection.find_one({"Title": item.Title}, {})
        if same is not None:
            raise HTTPException(detail="ItemList with this title already exist", status_code=400)

        item_list = ItemListModel(**{"Title": item.Title, "QueryKey": item.QueryKey})
        item_list = item_list_collection.insert_one(item_list.dict())
        return AddItemListResponseModel(**{"ItemId": str(item_list.inserted_id)})

    @staticmethod
    def get_by_title(title):
        item_list: dict = item_list_collection.find_one({"Title": title})
        if item_list is None:
            raise HTTPException(detail="ItemList with this title not found", status_code=400)
        return ItemListModel(**item_list)

    @staticmethod
    def get_by_title_load(title):
        item_list = item_list_collection.find_one({"Title": title})
        if item_list is None:
            raise HTTPException(detail="ItemList with this title not found", status_code=400)
        # todo : get token id
        data = Query.query_execution_by_key(item_list["QueryKey"], "ItemList", "", "")
        loaded = LoadItemListModel(**{"Title": item_list["Title"], "Data": data})
        return loaded

    @staticmethod
    def delete(title):

        item_list = item_list_collection.find_one({"Title": title})
        if item_list is None:
            raise HTTPException(detail="ItemList with this title not found", status_code=400)
        item_list_collection.delete_one({"Title": title})
        return DeleteItemListResponseModel(**{"ItemId": str(item_list["_id"])})

    @staticmethod
    def homepage_get_item_list(item_list_ld):
        item_list = item_list_collection.find_one({"_id": ObjectId(item_list_ld)}, {"Title"})
        if item_list is None:
            raise HTTPException(detail="Item list with this id not exist", status_code=400)

    @staticmethod
    def get_all():
        items = item_list_collection.find({})
        res = []
        for item in items:
            item["_id"] = str(item["_id"])
            res.append(item)
        return res
