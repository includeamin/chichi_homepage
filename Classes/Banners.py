from Classes.Tools import Tools
from Models.BannerModel import BannerModel , GetValidBannerDestination
from Models.RequestModel.BannerRequest import Add
from bson.objectid import ObjectId
from DataBase.DB import banner_collection
from Models.ResponseModel.Banner import Add as AddResponse, Delete ,GetAll
from fastapi import HTTPException

import logging


class Banners:
    valid_banners_destination = ["Category","Package","Product"]
    @staticmethod
    def add(input_data: Add):

        exist = banner_collection.find_one({"Name":input_data.Name},{})
        if exist is not None:
            raise HTTPException(detail="Banner already exist",status_code=400)
        if not ObjectId.is_valid(input_data.Image):
            raise HTTPException(detail=Tools.errors("IVI"),status_code=400)
        if not ObjectId.is_valid(input_data.DestinationId):
            raise HTTPException(detail=Tools.errors("IVI"),status_code=400)
        if not Banners.valid_banners_destination.__contains__(input_data.Destination):
            raise HTTPException(detail="Invalid Destination",status_code=400)

        banner = BannerModel(**{"Name":input_data.Name,"Image":input_data.Image,"Destination":input_data.Destination,"DestinationId":input_data.DestinationId})
        banner = banner_collection.insert_one(banner.dict())
        return AddResponse(**{"ItemId":str(banner.inserted_id)})

    @staticmethod
    def get(banner_name):
        item = banner_collection.find_one({"Name":banner_name})
        if item is None:
            raise HTTPException(detail=Tools.errors("INF"),status_code=400)
        item["Image"] = Tools.url_maker(item["Image"])
        return item

    @staticmethod
    def delete(banner_id):
        item = banner_collection.find_one({"_id": ObjectId(banner_id)})
        if item is None:
            raise HTTPException(detail=Tools.errors("INF"),status_code=400)
        banner_collection.delete_one({"_id":item["_id"]})
        return Delete(**{"ItemId": str(item["_id"])})

    @staticmethod
    def get_all():
        items = banner_collection.find({})
        res = []
        for item in items:
            item["Image"] = Tools.url_maker(item["Image"])
            item["_id"] = str(item["_id"])
            res.append(item)
        return GetAll(**{"Items":res})

    @staticmethod
    def get_valid_banners_destination():
        return GetValidBannerDestination(**{"ValidDestination":Banners.valid_banners_destination})

    @staticmethod
    def system_get_by_name(name):
        item = banner_collection.find_one({"Name":name})

        item["Image"] = Tools.url_maker(item["Image"])
        item["_id"]= str(item["_id"])

        return item


