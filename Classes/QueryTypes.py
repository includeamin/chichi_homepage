from Models.QueryTypeModel import QueryTypeModel,GetKeysModel
from DataBase.DB import query_types_collection
from fastapi import HTTPException
from Classes.Tools import Tools
from Models.ResponseModel.QueryTypes import AdminAddQueryType, AdminGetAllQueryType
import requests
import json


class Query:
    @staticmethod
    def admin_add(key, service, url, object_type):
        # todo : validate service name
        item = query_types_collection.find_one({"Key": key})
        if item is not None:
            raise HTTPException(status_code=500, detail=Tools.errors("IAE"))
        item = query_types_collection.insert_one(QueryTypeModel(**{"Key": key, "Service": service, "Url": url,
                                                                   "ObjectType": object_type}).dict())
        return AdminAddQueryType(**{"QueryId": str(item.inserted_id)})

    @staticmethod
    def admin_get_all():
        items = query_types_collection.find({})
        print("a")
        # res = [item for item in items]
        res = []
        for item in items:
            item["_id"] = str(item["_id"])
            res.append(item)
        return AdminGetAllQueryType(**{"Queries": res})

    @staticmethod
    def system_get_by_key(key, object_type):
        print(key, object_type)
        item = query_types_collection.find_one({"Key": key, "ObjectType": object_type})
        return item

    @staticmethod
    def query_url_maker(query: dict, token, id):
        # service/system/homepage/query/item-list/key
        url_format = f"{query['Service']}{query['Url']}{query['Key']}"
        a = requests.get(url_format, headers={"Token": token, "Id": id})

        if a.status_code == 200:
            a = json.loads(a.content)
            if not a["State"]:
                raise Exception(a["Description"])
            res = json.loads(a["Description"])
            return res
        else:
            raise Exception("HTTP EXCEPTION")

    @staticmethod
    def query_execution_by_key(key, object_type, token, id):
        item = Query.system_get_by_key(key, object_type)
        if item is None:
            raise Exception(Tools.errors("INF"))
        return Query.query_url_maker(item, token, id)

    @staticmethod
    def available_keys() -> GetKeysModel:
        items = query_types_collection.distinct("Key")
        return GetKeysModel(**{"Keys":items})
