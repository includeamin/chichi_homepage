from Classes.Tools import Tools
from DataBase.DB import homepage_collection, final_homepage_collection
from Models.HomePageModel import FinalHomePageModel, HomePageModel, ComponentModel, AddHomePageResponseModel, \
    AddComponentModel, UpdateHomePageWithJsonModel, GetAbvailableObjectType, GetAbvailableScreenPosition, \
    DeleteHomePageResponseModel, HomePageActiveModel, GetAllHomePageModel
from Models.ItemListModel import ItemListComponentDataModel
from Classes.ItemLists import ItemList
from fastapi import HTTPException
from datetime import datetime
from bson.objectid import ObjectId
from Classes.QueryTypes import Query
from Classes.HeaderSlider import HeaderSlider
from Classes.Slider import Slider
from Classes.Banners import Banners
from Classes.Categories import Category
from Classes.Packages import Package


class HomePage:
    available_object_type = ["HeaderSlider", "ItemList", "Slider",
                             "Package", "Category", "Banner"]
    available_screen_position = ["Header", "Body", "Footer"]

    @staticmethod
    def init_new_homepage(home_page_name):
        # exist = homepage_collection.find_one({"Name": home_page_name}, {})
        exist = homepage_collection.find_one({"Name": home_page_name}, {})
        if exist is not None:
            raise HTTPException(detail="Homepage with this name already exist", status_code=400)

        homepage = HomePageModel(**{"Name": home_page_name})
        homepage = homepage_collection.insert_one(homepage.dict())
        return AddHomePageResponseModel(**{"ItemId": str(homepage.inserted_id)})

    @staticmethod
    def update_homepage_with_json(homepage_json: UpdateHomePageWithJsonModel):
        homepage = homepage_collection.find_one({"Name": homepage_json.Name})
        if homepage is None:
            raise HTTPException(detail="Homepage with this name not exist", status_code=400)
        if not homepage_json.Header:
            raise HTTPException(detail="HomePage's Header can not empty", status_code=400)
        if not homepage_json.Body:
            raise HTTPException(detail="HomePage's Body can not empty", status_code=400)
        homepage_collection.update_one({"_id": homepage["_id"]}, {"$set": {"Header": homepage_json.Header,
                                                                           "Body": homepage_json.Body,
                                                                           "Footer": homepage_json.Footer,
                                                                           "Update_at": datetime.now()}})
        return AddHomePageResponseModel(**{"ItemId": str(homepage["_id"])})

    @staticmethod
    def active_component_that_used_in_homepage():
        # todo : develope in future
        pass

    @staticmethod
    def get_available_object_type():
        return GetAbvailableObjectType(**{"Items": HomePage.available_object_type})

    @staticmethod
    def get_available_screen_position():
        return GetAbvailableScreenPosition(**{"Items": HomePage.available_screen_position})

    @staticmethod
    def get_home_page_by_name(homepage_name):
        homepage = homepage_collection.find_one({"Name": homepage_name})
        if homepage is None:
            raise HTTPException(detail="Homepage with this name not exist", status_code=400)
        return HomePageModel(**homepage)

    @staticmethod
    def delete(homepage_name):
        homepage = homepage_collection.find_one({"Name": homepage_name}, {})
        if homepage is None:
            raise HTTPException(detail="Homepage with this name not exist", status_code=400)
        if homepage["IsUsed"]:
            raise HTTPException(detail="Used homepage cant delete", status_code=400)
        homepage_collection.delete_one({"_id": homepage["_id"]})
        return DeleteHomePageResponseModel(**{"ItemId": str(homepage["_id"])})

    @staticmethod
    def active(homepage_name):
        # todo : when active should be as e final
        # todo : check only one homepage in used
        homepage = homepage_collection.find_one({"Name": homepage_name}, {"IsUsed"})
        if homepage is None:
            raise HTTPException(detail="Homepage with this name not exist", status_code=400)
        same_used = homepage_collection.find_one({"IsUsed": True})
        # if same_used:
        #     raise HTTPException(detail="Two homepage can not in used at same time", status_code=400)
        homepage_collection.update_one({"_id": same_used["_id"]}, {"$set": {"IsUsed": False}})

        if homepage["IsUsed"]:
            is_used = False
        else:
            is_used = True
        homepage_collection.update_one({"_id": homepage["_id"]}, {"$set": {"IsUsed": is_used}})
        return HomePageActiveModel(**{"Action": "Active" if is_used else "DeActive"})

    @staticmethod
    def load_home_page(homepage_name):
        homepage = homepage_collection.find_one({"Name": homepage_name})
        if homepage is None:
            raise HTTPException(detail="Homepage with this name not exist", status_code=400)
        homepage["_id"] = str(homepage["_id"])

        for item in homepage["Header"]:
            if item["ObjectType"] == "ItemList":
                item["Data"]["Data"] = Query.query_execution_by_key("Best", 'ItemList', "", "id")
            else:
                item["Data"]["Data"] = HomePage.data_loader_depend_on_key(item["ObjectType"],
                                                                          item["Data"]["Title"])
        for item in homepage["Body"]:
            if item["ObjectType"] == "ItemList":
                item["Data"]["Data"] = Query.query_execution_by_key("Best", 'ItemList', "", "id")
            else:
                item["Data"]["Data"] = HomePage.data_loader_depend_on_key(item["ObjectType"],
                                                                          item["Data"]["Title"])
        for item in homepage["Footer"]:
            if item["ObjectType"] == "ItemList":
                item["Data"]["Data"] = Query.query_execution_by_key("Best", 'ItemList', "", "id")
            else:
                item["Data"]["Data"] = HomePage.data_loader_depend_on_key(item["ObjectType"],
                                                                          item["Data"]["Title"])
        return homepage


    @staticmethod
    def mobile_load_homepage():
        homepage = homepage_collection.find_one({"IsUsed": True})
        # homepage = homepage_collection.find_one({"Name": homepage_name})
        if homepage is None:
            raise HTTPException(detail="Homepage with this name not exist", status_code=400)
        homepage["_id"] = str(homepage["_id"])

        for item in homepage["Header"]:
            if item["ObjectType"] == "ItemList":
                item["Data"]["Data"] = Query.query_execution_by_key("Best", 'ItemList', "", "id")
                item["QueryKey"] = "Best"
            else:
                item["Data"]["Data"] = HomePage.data_loader_depend_on_key(item["ObjectType"],
                                                                          item["Data"]["Title"])
        for item in homepage["Body"]:
            if item["ObjectType"] == "ItemList":
                item["Data"]["Data"] = Query.query_execution_by_key("Best", 'ItemList', "", "id")
                item["QueryKey"] = "Best"

            else:
                item["Data"]["Data"] = HomePage.data_loader_depend_on_key(item["ObjectType"],
                                                                          item["Data"]["Title"])
        for item in homepage["Footer"]:
            if item["ObjectType"] == "ItemList":
                item["Data"]["Data"] = Query.query_execution_by_key("Best", 'ItemList', "", "id")
                item["QueryKey"] = "Best"

            else:
                item["Data"]["Data"] = HomePage.data_loader_depend_on_key(item["ObjectType"],
                                                                          item["Data"]["Title"])
        return homepage

    @staticmethod
    def get_all_homepage():
        homepages = homepage_collection.find({}, {"Name", "IsActive", "Update_at", "Create_at"})
        res = []
        for item in homepages:
            item["_id"] = str(item["_id"])
            res.append(item)
        return GetAllHomePageModel(**{"Items": res})

    @staticmethod
    def data_loader_depend_on_key(item, title):
        if item == "HeaderSlider":
            return HeaderSlider.system_get_by_name(title)
        elif item == "Slider":
            return Slider.system_get_by_name(title)
        elif item == "Package":
            return Package.system_get_by_name(title)
        elif item == "Category":
            return Category.system_get_by_name(title)
        elif item == "Banner":
            return Banners.system_get_by_name(title)

    # @staticmethod
    # # todo: impl later
    # def add_component_to_homepage(home_page_name, component: AddComponentModel, screen_position: str,
    #                               position: int = 0):
    #     homepage = homepage_collection.find_one({"Name": home_page_name})
    #     if homepage is None:
    #         raise HTTPException(detail="Homepage with this name already exist", status_code=400)
    #     if homepage["IsUsed"]:
    #         raise HTTPException(detail="In used homepage can not edit", status_code=400)
    #     if not HomePage.available_object_type.__contains__(component.ObjectType):
    #         raise HTTPException(detail="Invalid Object type ", status_code=400)
    #     if not ObjectId.is_valid(component.Id):
    #         raise HTTPException(detail="Invalid Id", status_code=400)
    #     if not HomePage.available_screen_position.__contains__(screen_position):
    #         raise HTTPException(detail="Invalid screen position", status_code=400)
    #
    #     # check item in that position exist or not
    #     last_position = 0
    #     aggr_result = homepage_collection.aggregate([
    #         {"$project": {f'Body.Position': 1}},
    #         {"$unwind": "$Body"},
    #         {"$sort": {"Body.Position": -1}},
    #         {"$limit": 1}
    #
    #     ])
    #
    #     if component.ObjectType == "ItemList":
    #         title = ItemList.homepage_get_item_list(component.Id)
    #         item_to_add = ComponentModel(**{"ObjectType": component.ObjectType, "Position": int(screen_position)})
    #     elif component.ObjectType == "HeaderSlider":
    #         pass
    #     elif component.ObjectType == "Slider":
    #         pass
    #     elif component.ObjectType == "Package":
    #         pass
    #     elif component.ObjectType == "Category":
    #         pass
    #     elif component.ObjectType == "Banner":
    #         pass
