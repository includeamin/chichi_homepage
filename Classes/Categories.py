from Models.CategoryModel import CategoryModel, AddCategoryModel, AddCategoryResponseModel, ImageList, \
    GetAllCategoryResponseModel, DeleteCategoryResponse
from DataBase.DB import category_collection
from bson import ObjectId
from fastapi import HTTPException
import json
from Classes.Tools import Tools


class Category:
    position_max_value = 4

    @staticmethod
    def add(name: str):
        exist = category_collection.find_one({"Name": name}, {})
        if exist is not None:
            raise HTTPException(detail="Item Already exist", status_code=400)
        category = CategoryModel(**{"Name": name})
        category = category_collection.insert_one(category.dict())
        return AddCategoryResponseModel(**{'ItemId': str(category.inserted_id)})

    @staticmethod
    def add_item(category_id, item_list: ImageList):
        category = category_collection.find_one({"_id": ObjectId(category_id)})
        if category is None:
            raise HTTPException(detail=Tools.errors("INF"), status_code=400)
        if not ObjectId.is_valid(item_list.Image):
            raise HTTPException(detail="Invalid image id", status_code=400)
        if not ObjectId.is_valid(item_list.DestinationId):
            raise HTTPException(detail="Invalid destination id", status_code=400)
        if 0 > item_list.Position > Category.position_max_value:
            raise HTTPException(detail="position number is out of range 0<=x<=4", status_code=400)

        category_collection.update_one({"_id": category["_id"], "Items.Position": int(item_list.Position)}, {
            "$set": {"Items.$.Image": item_list.Image, "Items.$.DestinationId": item_list.DestinationId}})
        return AddCategoryResponseModel(**{'ItemId': category_id})

    @staticmethod
    def get_all():
        res = []
        category = category_collection.find({})
        for item in category:
            category_res = []
            item["_id"] = str(item["_id"])
            for category_item in item["Items"]:
                category_item["Image"] = Tools.url_maker(category_item["Image"]) if category_item[

                                                                                        "Image"] is not None else Tools.url_maker(
                    Tools.default_image())
                category_res.append(category_item)

            item["Items"] = category_res
            res.append(item)
            print(res)

        return GetAllCategoryResponseModel(**{"Items": res})

    @staticmethod
    def get(category_name):
        package = category_collection.find_one({"Name": category_name})
        if package is None:
            raise HTTPException(detail="category not found", status_code=400)

        for item in package["Items"]:
            item["Image"] = Tools.url_maker(item["Image"]) if item["Image"] is not None else Tools.url_maker(
                Tools.default_image())

        return CategoryModel(**package)

    @staticmethod
    def delete(category_name):
        category = category_collection.find_one({"Name": category_name}, {})
        if category is None:
            raise HTTPException(detail="Package not found", status_code=400)
        category_collection.delete_one({"_id": category["_id"]})
        return DeleteCategoryResponse(ItemId=str(category["_id"]))

    @staticmethod
    def system_get_by_name(name):
        item = category_collection.find_one({"Name":name})
        for image in item["Items"]:
            image["Image"] = Tools.url_maker(image["Image"])
        item["_id"]= str(item["_id"])

        return item

