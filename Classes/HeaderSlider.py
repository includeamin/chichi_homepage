from Classes.Tools import Tools
from DataBase.DB import header_slider_collection
from Configs.ConfigsLoaders import available_destination
from bson.objectid import ObjectId
from Models.HeaderSliderModel import HeaderSliderModel, AddHeaderSliderModel, ImageList, GetAllHeaderSliderModel, \
    DeleteHeaderSliderModel, GetValidBannerDestination
from Models.ResponseModel.HeaderSliderResponseModel import Model, AdminGetAllMode, AdminDeleteModel, AdminGetModel
from fastapi import HTTPException


class HeaderSlider:
    position_max_value = 5
    valid_destination = ["Category", "Product", "Package"]

    @staticmethod
    def admin_add(image_id, destination, destination_id):
        try:

            if not available_destination.__contains__(destination):
                raise HTTPException(detail=Tools.errors("INCD"), status_code=400)
            if not ObjectId.is_valid(image_id):
                raise HTTPException(detail=Tools.errors("IID"), status_code=400)
            if not ObjectId.is_valid(destination_id):
                raise HTTPException(detail=Tools.errors("IDD"), status_code=400)
            header_slider = HeaderSliderModel(**{
                "Image": image_id,
                "Destination": destination,
                "DestinationId": destination_id
            })
            item = header_slider_collection.insert_one(header_slider.dict())
            return Model(**{"ItemId": str(item.inserted_id)})
        except HTTPException as ex:
            raise HTTPException(detail=ex.args, status_code=400)

    @staticmethod
    def add(header_slider_name, number: int):
        number = int(number)
        if 0 < HeaderSlider.position_max_value < number:
            raise HTTPException(
                detail=f"slider number can not bigger than {HeaderSlider.position_max_value} or lowe than 1",
                status_code=400)
        header_slider = header_slider_collection.find_one({"Name": header_slider_name})
        if header_slider is not None:
            raise HTTPException(detail="Header slider with this name already exist ", status_code=400)
        header_slider = HeaderSliderModel(
            **{"Name": header_slider_name, "Items": [ImageList(Position=i) for i in range(number)]})
        header_slider = header_slider_collection.insert_one(header_slider.dict())
        return AddHeaderSliderModel(**{"ItemId": str(header_slider.inserted_id)})

    @staticmethod
    def add_item(header_slider_name, item_list: ImageList):
        slider = header_slider_collection.find_one({"Name": header_slider_name})
        if slider is None:
            raise HTTPException(detail=Tools.errors("INF"), status_code=400)
        if not ObjectId.is_valid(item_list.Image):
            raise HTTPException(detail="Invalid image id", status_code=400)
        if not ObjectId.is_valid(item_list.DestinationId):
            raise HTTPException(detail="Invalid destination id", status_code=400)
        if 0 > item_list.Position > HeaderSlider.position_max_value:
            raise HTTPException(detail=f"position number is out of range 0<=x<={HeaderSlider.position_max_value}",
                                status_code=400)
        if not HeaderSlider.valid_destination.__contains__(item_list.Destination):
            raise HTTPException(detail=f"destination should be on of this:{str(HeaderSlider.valid_destination)}",
                                status_code=400)

        header_slider_collection.update_one({"_id": slider["_id"], "Items.Position": int(item_list.Position)}, {
            "$set": {"Items.$.Image": item_list.Image, "Items.$.DestinationId": item_list.DestinationId}})
        return AddHeaderSliderModel(**{'ItemId': str(slider["_id"])})

    @staticmethod
    def get_all():
        res = []
        slider = header_slider_collection.find({})
        for item in slider:
            slider_res = []
            item["_id"] = str(item["_id"])
            for header_slider_item in item["Items"]:
                header_slider_item["Image"] = Tools.url_maker(header_slider_item["Image"]) if header_slider_item[

                                                                                                  "Image"] is not None else Tools.url_maker(
                    Tools.default_image())
                slider_res.append(header_slider_item)

            item["Items"] = slider_res
            res.append(item)
        return GetAllHeaderSliderModel(**{"Items": res})

    @staticmethod
    def get(header_slider_name):
        slider = header_slider_collection.find_one({"Name": header_slider_name})
        if slider is None:
            raise HTTPException(detail="slider not found", status_code=400)

        for item in slider["Items"]:
            item["Image"] = Tools.url_maker(item["Image"]) if item["Image"] is not None else Tools.url_maker(
                Tools.default_image())

        return HeaderSliderModel(**slider)

    @staticmethod
    def delete(header_slider_name):
        slider = header_slider_collection.find_one({"Name": header_slider_name}, {})
        if slider is None:
            raise HTTPException(detail="slider not found", status_code=400)
        header_slider_collection.delete_one({"_id": slider["_id"]})
        return DeleteHeaderSliderModel(ItemId=str(slider["_id"]))

    @staticmethod
    def admin_get_by_id(header_slider_id):
        try:
            if not ObjectId.is_valid(header_slider_id):
                raise HTTPException(detail=Tools.errors('IVI'), status_code=400)
            item = header_slider_collection.find_one({"_id": ObjectId(header_slider_id)})
            if item is None:
                raise HTTPException(detail=Tools.errors("INF"), status_code=400)
            item["Image"] = Tools.url_maker(item["Image"])
            return HeaderSliderModel(**item)
        except HTTPException as ex:
            raise HTTPException(detail=ex.args, status_code=400)

    @staticmethod
    def admin_get_all():
        try:
            items = header_slider_collection.find({})
            res = []
            for item in items:
                item["_id"] = str(item["_id"])
                res.append(item)
            return AdminGetAllMode(Headers=res)
        except Exception as ex:
            return Tools.Result(False, ex.args)

    @staticmethod
    def admin_delete_by_id(header_slider_id):
        try:
            if not ObjectId.is_valid(header_slider_id):
                raise Exception(Tools.errors('IVI'))
            header_slider_collection.delete_one({"_id": ObjectId(header_slider_id)})
            return AdminDeleteModel(**{"Message": "Done"})
        except Exception as ex:
            return Tools.Result(False, ex.args)

    @staticmethod
    def get_valid_banners_destination():
        return GetValidBannerDestination(**{"ValidDestination": HeaderSlider.valid_destination})

    @staticmethod
    def system_get_by_name(name):
        item = header_slider_collection.find_one({"Name": name})
        for image in item["Items"]:
            image["Image"] = Tools.url_maker(image["Image"])
        item["_id"] = str(item["_id"])
        return item
