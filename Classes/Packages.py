from Models.PackageModel import PackageModel, AddPackageRequest, ImageList, GetAllPackageResponse, DeletePackageRequest
from Classes.Tools import Tools
from DataBase.DB import package_collection
from bson import ObjectId
from fastapi import HTTPException


class Package:
    position_max_value = 5

    @staticmethod
    def add(name: str):
        exist = package_collection.find_one({"Name": name}, {})
        if exist is not None:
            raise HTTPException(detail="Item Already exist", status_code=400)
        package = PackageModel(**{"Name": name})
        package = package_collection.insert_one(package.dict())
        return AddPackageRequest(**{'ItemId': str(package.inserted_id)})

    @staticmethod
    def add_item(package_id, item_list: ImageList):
        package = package_collection.find_one({"_id": ObjectId(package_id)})
        if package is None:
            raise HTTPException(detail=Tools.errors("INF"), status_code=400)
        if not ObjectId.is_valid(item_list.Image):
            raise HTTPException(detail="Invalid image id", status_code=400)
        if not ObjectId.is_valid(item_list.DestinationId):
            raise HTTPException(detail="Invalid destination id", status_code=400)
        if 0 > item_list.Position > 5:
            raise HTTPException(detail=f"position number is out of range 0<=x<={Package.position_max_value}", status_code=400)

        package_collection.update_one({"_id": package["_id"], "Items.Position": int(item_list.Position)}, {
            "$set": {"Items.$.Image": item_list.Image, "Items.$.DestinationId": item_list.DestinationId}})
        return AddPackageRequest(**{'ItemId': package_id})

    @staticmethod
    def get(package_name):
        package = package_collection.find_one({"Name": package_name})
        if package is None:
            raise HTTPException(detail="Package not found", status_code=400)

        for item in package["Items"]:
            item["Image"] = Tools.url_maker(item["Image"]) if item["Image"] is not None else Tools.url_maker(
                Tools.default_image())

        return PackageModel(**package)

    @staticmethod
    def get_all():
        res = []
        package = package_collection.find({})
        for item in package:
            package_res = []
            item["_id"] = str(item["_id"])
            for package_item in item["Items"]:
                package_item["Image"] = Tools.url_maker(package_item["Image"]) if package_item[
                                                                                      "Image"] is not None else Tools.url_maker(
                    Tools.default_image())
                package_res.append(package_item)

            item["Items"] = package_res
            res.append(item)

        return GetAllPackageResponse(**{"Items": res})

    @staticmethod
    def update(package_id, new_package):
        pass

    @staticmethod
    def delete(package_id):
        package = package_collection.find_one({"_id": ObjectId(package_id)}, {})
        if package is None:
            raise HTTPException(detail="Package not found", status_code=400)
        package_collection.delete_one({"_id": package["_id"]})
        return DeletePackageRequest(ItemId=package_id)
    @staticmethod
    def system_get_by_name(name):
        item = package_collection.find_one({"Name":name})
        for image in item["Items"]:
            image["Image"] = Tools.url_maker(image["Image"])
        item["_id"]= str(item["_id"])

        return item

