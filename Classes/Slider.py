from Models.SliderModel import GetAllSliderResponse, SliderModel, ImageList, AddSliderResponse, DeleteSliderResponse, \
    GetValidBannerDestination
from Classes.Tools import Tools
from DataBase.DB import slider_collection
from fastapi import HTTPException
from bson.objectid import ObjectId


class Slider:
    valid_destination = ["Category", "Package", "Product"]
    position_max_value = 5

    @staticmethod
    def add(slider_name, number: int):
        number = int(number)
        slider = slider_collection.find_one({"Name": slider_name})
        if 2 < Slider.position_max_value < number:
            raise HTTPException(
                detail=F"Number of slider is bigger than max value {Slider.position_max_value} or lower than 2",
                status_code=400)
        if slider is not None:
            raise HTTPException(detail="Slider with this name already exist", status_code=400)
        slider = SliderModel(**{"Name": slider_name, "Items": [ImageList(Position=i) for i in range(number)]}).dict()
        slider = slider_collection.insert_one(slider)
        return AddSliderResponse(**{"ItemId": str(slider.inserted_id)})

    @staticmethod
    def add_item(slider_name, item_list: ImageList):
        slider = slider_collection.find_one({"Name": slider_name})
        if slider is None:
            raise HTTPException(detail=Tools.errors("INF"), status_code=400)
        if not ObjectId.is_valid(item_list.Image):
            raise HTTPException(detail="Invalid image id", status_code=400)
        if not ObjectId.is_valid(item_list.DestinationId):
            raise HTTPException(detail="Invalid destination id", status_code=400)
        if 0 > item_list.Position > Slider.position_max_value:
            raise HTTPException(detail=f"position number is out of range 0<=x<={Slider.position_max_value}",
                                status_code=400)
        if not Slider.valid_destination.__contains__(item_list.Destination):
            raise HTTPException(detail=f"destination should be on of this:{str(Slider.valid_destination)}",
                                status_code=400)

        slider_collection.update_one({"_id": slider["_id"], "Items.Position": int(item_list.Position)}, {
            "$set": {"Items.$.Image": item_list.Image, "Items.$.DestinationId": item_list.DestinationId}})
        return AddSliderResponse(**{'ItemId': str(slider["_id"])})

    @staticmethod
    def get_all():
        res = []
        slider = slider_collection.find({})
        for item in slider:
            slider_res = []
            item["_id"] = str(item["_id"])
            for slider_item in item["Items"]:
                slider_item["Image"] = Tools.url_maker(slider_item["Image"]) if slider_item[

                                                                                    "Image"] is not None else Tools.url_maker(
                    Tools.default_image())
                slider_res.append(slider_item)

            item["Items"] = slider_res
            res.append(item)
        return GetAllSliderResponse(**{"Items": res})

    @staticmethod
    def get(slider_name):
        slider = slider_collection.find_one({"Name": slider_name})
        if slider is None:
            raise HTTPException(detail="slider not found", status_code=400)

        for item in slider["Items"]:
            item["Image"] = Tools.url_maker(item["Image"]) if item["Image"] is not None else Tools.url_maker(
                Tools.default_image())

        return SliderModel(**slider)

    @staticmethod
    def delete(slider_name):
        slider = slider_collection.find_one({"Name": slider_name}, {})
        if slider is None:
            raise HTTPException(detail="slider not found", status_code=400)
        slider_collection.delete_one({"_id": slider["_id"]})
        return DeleteSliderResponse(ItemId=str(slider["_id"]))

    @staticmethod
    def get_valid_banners_destination():
        return GetValidBannerDestination(**{"ValidDestination": Slider.valid_destination})

    @staticmethod
    def system_get_by_name(name):
        item = slider_collection.find_one({"Name": name})
        for image in item["Items"]:
            image["Image"] = Tools.url_maker(image["Image"])
        item["_id"] = str(item["_id"])
        return item
