##FROM ubuntu:18.04
##
##EXPOSE 3006
##
##RUN apt-get update -y && \
##    apt-get install -y python3 python3-dev python3-pip
##
##
##RUN apt-get install -y sudo curl wget locales
##RUN locale-gen en_CA.UTF-8
##ENV LC_ALL=en_CA.UTF-8
##ENV LANG=en_CA.UTF-8
##ENV LANGUAGE=en_CA.UTF-8
##
### We copy just the requirements.txt first to leverage Docker cache
##COPY ./requirements.txt ./app/requirements.txt
##
##COPY . ./app
##
##
##
##WORKDIR /app
##
##
##RUN pip3  install -r requirements.txt
##RUN pip3 install uvicorn
##RUN pip3 install fastapi
##RUN pip3 install fastapi[all]
##RUN pip3 install email-validator
##RUN pip3 install pymongo
##RUN pip3 install gunicorn
##
##
##
##
##
##
###CMD ["python3","-u","app.py"]
##CMD ["/usr/local/bin/gunicorn" , "app:app","-w","4", "-k", "uvicorn.workers.UvicornWorker","--bind","0.0.0.0:3006","--log-level","debug","--threads=2"]
###CMD ["/usr/local/bin/uvicorn","--host","0.0.0.0" ,"app:app","--port","3005"]
###CMD ["uvicorn","--host","0.0.0.0" ,"app:app","--port","3006"]
#
#FROM includeamin/ubuntu_fastapi:latest
#
#EXPOSE 3006
##
##RUN apt-get update -y && \
##    apt-get install -y python3 python3-dev python3-pip
#
#
##RUN apt-get install -y sudo curl wget locales
##RUN locale-gen en_CA.UTF-8
##ENV LC_ALL=en_CA.UTF-8
##ENV LANG=en_CA.UTF-8
##ENV LANGUAGE=en_CA.UTF-8
#
## We copy just the requirements.txt first to leverage Docker cache
##COPY ./requirements.txt ./app/requirements.txt
#RUN mkdir app
#COPY . ./app
#
#
##WORKDIR /app
#
##
##RUN pip3  install -r requirements.txt
##RUN pip3 install uvicorn
##RUN pip3 install fastapi
##RUN pip3 install fastapi[all]
##RUN pip3 install email-validator
##RUN pip3 install pymongo
##RUN pip3 install gunicorn
##
#RUN cd app && ls
#
#
#
#
#WORKDIR app
#
##RUN cd Database && ls
##CMD ["python3","-u","app-old.py"]
##CMD ["/usr/local/bin/gunicorn", "--config", "gunicorn_config.py" , "app:app","uvicorn.workers.UvicornWorker"]
##CMD ["/usr/local/bin/uvicorn","--host","0.0.0.0" ,"app:app","--port","3005"]
#CMD ["/usr/local/bin/gunicorn" , "app:app","-w","4", "-k", "uvicorn.workers.UvicornWorker","--bind","0.0.0.0:3006","--log-level","debug","--threads=2"]
#
##CMD ["uvicorn","--host","0.0.0.0" ,"app:app","--port","3002"]

FROM includeamin/ubuntu_fastapi:v2

EXPOSE 3006
COPY . app
WORKDIR app
RUN ls

CMD ["gunicorn" , "app:app","-w","2", "-k", "uvicorn.workers.UvicornWorker","--bind","0.0.0.0:3006","--log-level","debug","--threads=2"]